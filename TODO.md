
#@TODOs
- Cleanup (remove extra code, make everything look nice, add comments to explain where neccesary)
- add error catching for when the location of a module is (0, 0)
##Add additional features like:
- add an offset to help compensate for drift while translating and rotating at the same time
- simulate different drive bases like ackerman and tank with the swerve drive

#Unintelligible Random Ideas
One idea/speculation is to rarefactor all of the swerve drive code to be a generic propeller drive the idea being that really all the swerve drive code is, is calculating the optimum desired velocity of things like wheels that propel an entire body to a desired velocity. So whats to stop us from adding a 3rd degree of freedom and code to acount for restrictions on the direction of propellers and using it pilot flying vehicles or tank drives with statically oriented propellers. 