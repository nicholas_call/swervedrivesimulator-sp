
# Brief
My purpose in this codebase is to simulate a functioning swerve drive. To accomplish this I'm creating a virtual swerve drive in Bullet Physics and gathering gamepad inputs using XInput. The code that actually does the calculations for the swerve drive is located under the src/swerve directory.

# Building The Source
## Requirements:
 - Windows operating system >= 8 (note: this requirement only exists because xinput is being used to capture gamepad inputs. Given some source modifications it's likely this could easily build on mac and linux as well)
 - ~1.3 ish gigabytes of free space (for the Bullet Physics repo)


## Steps:
1. Get a c++ compiler (personally I've found the easiest thing is to simply download visual studio and install their c++ compiler within the visual studio installer. The version I used was 14.22.278)
2. Get cmake from [here](https://cmake.org/download/) for windows
3. Get the Bullet Physics repo

    * #### Quick explanation as to why I'm resorting to this questionable method of building code:
      As I understand it currently (as of july 2019) the authors of Bullet Physics have been focusing on their python API (which is amazing and I probably would have written this entire thing using it if it wasn’t for 4061 using a c++ codebase. It can be found [here](https://pybullet.org/wordpress/)) which combined with not having enough people has resulted in many features existing in the examples directory for the sake of convenience. This unfortunately means it's rather difficult to build the source and just include the built binaries and header files if you want to take advantage of all of the features. The work around I've resorted to is just to include this repo as one of the example projects in the Bullet Physics repo. It's not the prettiest and is likely not the best work around but it works.

    To start, download the Bullet Physics repo (via zip, git, etc) from [here](https://github.com/bulletphysics/bullet3). Then move the swervedrivesimulator-sp directory obtained by downloading this repo to name_of_the_root_directory_of_bullet_physics\examples\swervedrivesimulator-sp. Lastly edit name_of_the_root_directory_of_bullet_physics\examples\CMakeLists.txt and add the text 'swervedrivesimulator-sp' to the SUBDIR() parentheses. 
4. Now if you configure and build with cmake from the     name_of_the_root_directory_of_bullet_physics directory this repo should     also build. Personally I used vscode with the cmake extension to run the    actual build commands.

# Running
To actually run the executable requires that the model files from the models directory and Bullet Physics data directory are in the same directory as the executable. It should be set up in the cmake to transfer those files over automatically already but I have had some problems with it not working if for example I used visual studios itself to build the code versus my regular build system. If all else fails just copying the models into the same directory should work. If you do end up resorting to this method make sure to grab both the plane.urdf and plane.obj files from the Bullet Physics data directory as well (these are required to load the basic field).

Once all the model files and executable are in the same directory it's possible to to simply click the executable and it will run with the default arguments. To run the executable with custom args i.e. if you want to tell it to load the 2018 4061 robot and/or the 2018 field the args format is as follows. The program will take either zero args and it will run the default field and robot or it will take 2 args. These args can be either basic or 2018. The first of the two args is for the robot while the second is the field. Use basic if you want the default field or robot (technically any string would work). Use 2018 to specify you want the 2018 4061 robot or 2018 field.  
 ### Examples:
 - The command “./executableName.exe basic basic“ is identical to running it with no args.  
 - The command “./executableName.exe 2018 basic” will load the 2018 robot and the default field.

# Controls
Once running the program should load up the field and robot. After that point one can control the demo in the two ways described below. 

But first, In addition to controlling the swerve drive there exist some useful controls built into the example browser including:
   - **s**: toggle shadows: useful if the frame rate is running badly 
   - **mouse scroll**: zoom in and out
   - **alt + mouse left click (only when camera follow on the robot is disabled)**: rotate camera
   - **alt + middle mouse click  (only when camera follow on the robot is disabled)**: translate the camera

---

- **Gamepad (preferred)**: Using a gamepad such as an xbox controller or any controller that supports xinput one can take full advantage of all the controls there are to use. Below are the bindings on a typical gamepad.
    - **right shoulder**: toggles camera follow (default: on)
    - **left shoulder**: toggles whether or not the robot will drive in a field oriented way (default: on)
    - **left joystick**: translation control
    - **right joystick**: rotation control
    - **d-pad**: pressing different directions on the dpad will change the field oriented direction for forward (default: up)
    - **x (2018 robot only)**: load cube: to load a cube first line up the robot in front of a cube with the arms full extended. Then   bring the arms together at the same time. Once the arms are touching the cube you can press x which will tell the intake rollers   on the arms to turn on and the ramp rollers to open up to take a cube. After taking in a cube stop pressing x and the ramp rollers will come together holding the cube in place. You're now ready to shoot the cube!
    - **a (2018 robot only)**: shoot: tells ramp rollers to run, ejecting any cube between them
    - **b (2018 robot only)**: slow shoot: dido the a button but shoots slower
    - **y (2018 robot only)**: reverse slow shoot: tells the ramp rollers to reverse
    - **left trigger (2018 robot only)**: controls the position of the left arm
    - **right trigger (2018 robot only)**: controls the position of the right arm

- **Keyboard**: basic keyboard controls for driving are supported but there isn't support for the extra features of the 2018 robot. Note, the controls shown below are disabled when a gamepad is plugged in.
    - **u h j k**: translation control: the example browser that is being used for this demo already has things bound to w a s d (otherwise known as the traditional translation keybinding) so instead it's been mapped to u h j k.
    - **z**: rotate left
    - **x**: rotate right
    - **t**: toggles camera follow (default: on)
    - **c**: toggles whether or not the robot will drive in a field oriented way (default: on) 



