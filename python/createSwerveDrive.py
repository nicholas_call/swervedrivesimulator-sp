import uuid
import random



number_of_modules = 20




def getStrOfFile(filepath):
    file = ""

    with open(filepath, 'r') as fh:
        file = fh.read()   # reads all the lines into a list
    
    return file


module_template = getStrOfFile("module.txt") + "MODULES"
finale_str = getStrOfFile("base.txt")



for i in range(number_of_modules):
    print("creating module" + str(i))

    x = random.randrange(-4, 4)
    y = -0.1
    z = random.randrange(-4, 4)

    finale_str = finale_str.replace("MODULES", module_template.replace("-1_SM", str(i) + "_SM").replace("MODULE_LOCATION", str(x) + " " + str(y) + " " + str(z)))

    




finale_str = finale_str.replace("MODULES", " ")







unique_filename = "swerve_robot" + str(uuid.uuid4()) + ".urdf"




# write to file:
open(unique_filename,'w').write(finale_str)