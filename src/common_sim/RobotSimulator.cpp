// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "../../../RobotSimulator/b3RobotSimulatorClientAPI.h"
#include "../Utils/b3Clock.h"
#include "../common/Gamepad.h"
#include <string.h>
#include <iostream>

#include <assert.h>
#include <filesystem>
#include <math.h>

#include <windows.h>
#include "shlwapi.h"
#include <xinput.h>

#include "RobotSimulator.h"





//////////////////////////////////////////////////////////////////////////
RobotSimulator::RobotSimulator(class b3RobotSimulatorClientAPI_NoGUI* sim)
{
	m_simClient = sim;
}



//////////////////////////////////////////////////////////////////////////
void
RobotSimulator::loadField(std::string arg)
{

	std::cout << "Overrid Me!" __FILE__ << "(" << __LINE__ << ")" << std::endl;

}


//////////////////////////////////////////////////////////////////////////
void
RobotSimulator::addRobot(Robot* robot)
{

	m_robots.push_back(robot);

}

    

//////////////////////////////////////////////////////////////////////////
void
RobotSimulator::runRobotTasks(void)
{
	for (int x = 0; x < m_robots.size(); x++)
	{
		m_robots[x]->runTasks(m_simClient);
	}
}


//////////////////////////////////////////////////////////////////////////
void 
RobotSimulator::go(void)
{
	std::cout << "Overrid Me!" __FILE__ << "(" << __LINE__ << ")" << std::endl;


}



////////////////////////////////////////////////////////////////////////////////
std::map<std::string, int>
RobotSimulator::mapNameToId(class b3RobotSimulatorClientAPI_NoGUI* sim, int bodyId)
{
	
	int numJoints = sim->getNumJoints(bodyId);
	

	
	std::map<std::string, int> map;

	for (int i = 0; i < numJoints; i++)
	{	
		
		b3JointInfo* info = new b3JointInfo();
		sim->getJointInfo(bodyId, i, info);

		map[info->m_jointName] = i;
	}

	return map;
}
