// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "../../../RobotSimulator/b3RobotSimulatorClientAPI.h"
#include "../Utils/b3Clock.h"
#include "../common/Gamepad.h"
#include <string.h>
#include <iostream>

#include <assert.h>
#include <filesystem>
#include <math.h>

#include <windows.h>
#include "shlwapi.h"
#include <xinput.h>

#include "Robot.h"


//////////////////////////////////////////////////////////////////////////
Robot::Robot(void)
{
	// intentionally left empty
}


//////////////////////////////////////////////////////////////////////////
void 
Robot::runTasks(class b3RobotSimulatorClientAPI_NoGUI* sim)
{
	std::cout << "Overrid Me!" __FILE__ << "(" << __LINE__ << ")" << std::endl;
}

//////////////////////////////////////////////////////////////////////////
void Robot::configure(std::string arg, b3RobotSimulatorClientAPI_NoGUI* sim, btVector3 starting_location, btQuaternion starting_orientation)
{
	std::cout << "Overrid Me!" __FILE__ << "(" << __LINE__ << ")" << std::endl;
}

   

