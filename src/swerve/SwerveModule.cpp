// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////

#include <cstdio>
#include <math.h>  
#include "../common/PsbPidController.h"
#include "SwerveModule.h"
#include "SwerveMotor.h"
#include "../common/DegreeCalc.h"

#include <iostream>
#include <string>


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define M_PI         3.14159265358979323846  // pi

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Drivebase {

////////////////////////////////////////////////////////////////////////////////
SwerveModule::SwerveModule(
      Vector2 location
   ,  PsbPidController *rotatePIDController
   ,  SwerveMotor *drive_motor
   ,  SwerveMotor *rotate_motor) 
   :
   m_desired_angle(0),
   m_drive_motor(drive_motor),
   m_rotate_motor(rotate_motor),
   m_rotatePID(rotatePIDController)
{
   
   setLocation(location);
   
   
}

////////////////////////////////////////////////////////////////////////////////
SwerveModule::SwerveModule(void) 
{
   // Intentionally left empty

}


////////////////////////////////////////////////////////////////////////////////
SwerveModule::~SwerveModule(void) 
{
   delete m_drive_motor;
   delete m_rotate_motor;
}


////////////////////////////////////////////////////////////////////////////////
PsbPidController *
SwerveModule::createPID(
      float kp,
      float ki,
      float kd,
      float min_out,
      float max_out,
      float minIntegralSum,
      float maxIntegralSum,
      float innerEpsilon,
      float outerEpsilon)
{
 return new PsbPidController(
                                    kp
                                 ,  ki
                                 ,  kd
                                 ,  0
                                 ,  0    
                                 ,  360   
                                 ,  min_out  
                                 ,  max_out  
                                 ,  minIntegralSum     
                                 ,  maxIntegralSum     
                                 ,  innerEpsilon   
                                 ,  outerEpsilon   
                                 ,  true
   );

}


////////////////////////////////////////////////////////////////////////////////
double
SwerveModule::getCurrentAngle(bool acountForReversed)
{
   double angle = DegreeCalc::normalizeDegree((double) (m_rotate_motor->getTicks() - m_forward_tick_value) * m_ticks_per_revolution_mulitpier);


   if (acountForReversed)
   {
      return m_reversed ? 
      DegreeCalc::normalizeDegree((double) angle - 180) 
      : 
      angle;
   }
   else
   {
      return angle;
   }
   

}

////////////////////////////////////////////////////////////////////////////////
void SwerveModule::enableStateChanged(bool newState)
{
   if (newState == isEnabled())
   {
      return;
   }


   if (newState)
   {
      
      m_rotatePID->enable();//don't really need to do this
   }
   else
   {
     
      m_rotatePID->disable();//don't really need to do this
      m_drive_motor = 0;
      m_rotate_motor = 0;

   }
}



////////////////////////////////////////////////////////////////////////////////
Vector2
SwerveModule::getLocation(void)
{
  
   return m_location.copy();
}

////////////////////////////////////////////////////////////////////////////////
Vector2
SwerveModule::getLocation(Vector2 origin)
{
  
   return getLocation() - origin;
}
 
////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::setLocation(Vector2 location)
{
   m_location = location;
  
}

////////////////////////////////////////////////////////////////////////////////
SwerveMotor*
SwerveModule::getDriveMotor(void)
{
   return m_drive_motor;
}

////////////////////////////////////////////////////////////////////////////////
SwerveMotor*
SwerveModule::getRotateMotor(void)
{
   return m_rotate_motor;
}

////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::setTickValueForForward(double ticks)
{
   m_forward_tick_value = ticks;
}


////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::setTicksPerRotation(double ticksInOneRotation)
{
   m_ticks_per_revolution_mulitpier = 360 / ticksInOneRotation;
}


////////////////////////////////////////////////////////////////////////////////
double
SwerveModule::getDesiredAngle(void)
{
   return m_desired_angle;
}

////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::setVelocity(double drive_throttle, double new_angle)
{
   m_desired_angle = DegreeCalc::normalizeDegree(new_angle);//set new angle


   // if were not enabled do nothing!
   if (!m_enabled)
   {
      m_drive_motor->set(0);
      m_rotate_motor->set(0);
      return;
   }


   //--------------------------------
   // Calculate Rotate Motor Output
   //-------------------------------

   double current_angle = getCurrentAngle();
   double current_reversedAngle = DegreeCalc::normalizeDegree(current_angle - 180);

   double angleDiff = DegreeCalc::getAngleDistance(current_angle, m_desired_angle);
   double angleReversedDiff = DegreeCalc::getAngleDistance(current_reversedAngle, m_desired_angle);


   if (angleDiff >= angleReversedDiff) // if just rotating normally is farther than reversing then reverse!
   {
   
      //swap everything                    
      if (m_reversed)
      {
         m_reversed = false;                        
      }                    
      else if (!m_reversed)
      {
         m_reversed = true;
      }
      
      //reset pid if toggle?
      

      // adjust throttle so we arent driving 100% till we are actually pointing the correct direction
      drive_throttle *= cos(angleReversedDiff * M_PI / 180);

   }
   else
   {
      // adjust throttle so we arent driving 100% till we are actually pointing the correct direction
      drive_throttle *= cos(angleDiff * M_PI / 180);
    
   }
   
   

   
   // apply rotate to motor with pid
   m_rotatePID->setSetPoint(m_desired_angle);
   m_rotate_motor->set(m_rotatePID->calcPid(getCurrentAngle()));// in order to be counterclockwise positive this would need to be multiplied by negative 1 or the positive direction of the encoder would need to be swapped

 
 
   //-------------------------------
   // Calculate Drive Motor Output
   //-------------------------------
   
   
   if (m_reversed)
   {
      m_drive_motor->set(drive_throttle * -1);
   }      
   else
   {
      m_drive_motor->set(drive_throttle);
   }
      
   
   
 
}
 

////////////////////////////////////////////////////////////////////////////////
void
SwerveModule::updateState(void)
{
   m_prev_angle = getCurrentAngle(false);
   m_prev_drive_encoder_counts = m_drive_motor->getTicks();
  
}


////////////////////////////////////////////////////////////////////////////////
Vector2
SwerveModule::getDisplacement(void)
{
  
   double angle = M_PI / 180 * DegreeCalc::normalizeDegree((getCurrentAngle(false) + m_prev_angle) / 2); //take average of now and prev degree, convert to radians
   double distance = (m_drive_motor->getTicks() - m_prev_drive_encoder_counts);

   //std::cout <<"dist:" + std::to_string(distance) + "\t Angle:" + std::to_string(angle * 180 / M_PI);
  // std::cout <<"\t ticks:" + std::to_string(m_drive_motor->getTicks()) + "\t speed:" + std::to_string(m_drive_motor->getSpeed()) + "\t Rev:" + std::to_string(m_reversed)<< std::endl;
 
   Vector2 displacement;

   


   if (distance != 0)
   {
      // convert to x, y format where forward is zero and clockwise is positive
      displacement = Vector2(sin(angle) * distance, cos(angle) * distance);

   
   }
   

  
   return displacement;
}


////////////////////////////////////////////////////////////////////////////////
Vector2
SwerveModule::calculateOptimumTurnVelocity(Vector2 rotate_origin)
{
   Vector2 temp_location = getLocation(rotate_origin);

   double r = temp_location.magnitude();
   
   // if the module isn't on the point of origin (r == 0) then
   if (r != 0)
   {


      // normalize rotate vector so that the magnitude is 1
      Vector2 temp = temp_location.getNormalized();


      // we swap the x, y axis of the rotate vector and multiplying the new y by -1 to spin it to optimum turning angle
      return Vector2(temp[1], temp[0] * -1);


   }
   else // not anything the module can do so go to the default location
   {
      return Vector2(0, 0);
   }
   
   

}

}
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

