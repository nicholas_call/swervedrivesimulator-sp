// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////

#include "SwerveModule.h"
#include "SwerveDrive.h"
#include "../common/DegreeCalc.h"
#include <string> // might not need this

#include <iostream>
#include <vector>
#include "../common/Vector2.h"





// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define M_PI         3.14159265358979323846  // pi

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Drivebase {



////////////////////////////////////////////////////////////////////////////////
SwerveDrive::SwerveDrive(bool enabled, std::vector<Psb::Drivebase::SwerveModule*> modules) : SwerveDrive(enabled)
{
   
   // not sure why I cant just say vector = vector but adding each module works too
   for (auto module : modules)
   {
      m_modules.push_back(module);
   }

}

////////////////////////////////////////////////////////////////////////////////
SwerveDrive::SwerveDrive(void)
{
   // Intentionally left empty
}

////////////////////////////////////////////////////////////////////////////////
SwerveDrive::SwerveDrive(bool enabled)
{
   setEnabled(enabled);
}

////////////////////////////////////////////////////////////////////////////////
SwerveDrive::~SwerveDrive(void)
{
   //@TODO delete everything!
}


////////////////////////////////////////////////////////////////////////////////
void SwerveDrive::enableStateChanged(bool newState)
{
   // if it wasn't anything new don't worry about it
   if (newState == isEnabled())
   {
      return;
   }

   // update module states to match
   for (auto module : m_modules)
   {
      module->setEnabled(newState);
   }
   

   
}

//////////////////////////////////////////////////////////////////////////
void SwerveDrive::resetPIDs(void)
{
   // @TODO
}


//////////////////////////////////////////////////////////////////////////
std::vector<Psb::Drivebase::SwerveModule*> SwerveDrive::getModules(void)
{
   return m_modules;
}


//////////////////////////////////////////////////////////////////////////
void SwerveDrive::addModule(Psb::Drivebase::SwerveModule* new_module)
{
   
   new_module->setEnabled(isEnabled());
   m_modules.push_back(new_module);
   
}


//////////////////////////////////////////////////////////////////////////
void SwerveDrive::setRotateOrigin(double x, double y)
{
   m_rotate_origin[0] = x;
   m_rotate_origin[1] = y;
}

//////////////////////////////////////////////////////////////////////////
void SwerveDrive::setRotateOrigin(Vector2 origin)
{
   m_rotate_origin = origin;
   
}


////////////////////////////////////////////////////////////////////////////////
void
SwerveDrive::set(double STR, double FWD, double RCW, double GYRO)
{
   

   int numModules = m_modules.size();
  
   // if disabled or no modules
   if (!isEnabled() || numModules <= 0)
   {
      // intentionally left empty

      return;
   }
  
   

  
   Vector2 trans_v = Vector2(STR, FWD);

   // if field centric then rotate the translate vector 
   // Note: this code expects the gyro to be positive clockwise and be a degree
   if (GYRO != NULL)
   {
      double GRYO_radi = (M_PI /180) * DegreeCalc::normalizeDegree(GYRO);// cpp takes radians in cos, sin, and etc. So convert it to radians

      trans_v = trans_v.getRotated(GRYO_radi);

      
     
   }

   //std::cout << "STR:" << (float)STR << " FWD:" << (float)FWD << " RCW:" << (float)RCW << " GYRO:" << GYRO << std::endl;

   // rotate the translate vector 90 degrees multiplied by how much rotate is desired and the offset. @TODO shouldn't this have an extra negative?
   trans_v = trans_v.getRotated((M_PI / 2) * RCW * m_driftOffset);
 
     
  


   // find the farthest distance a module is from the rotate origin
   // this is needed to scale rotating so closer wheels to the origin go slower while farther out ones go faster making for a more stable turn 
   double farthestDistance = 0;

   for (auto module : m_modules)
   {
    
      double dist = module->getLocation(m_rotate_origin).magnitude();

    
      
      if (farthestDistance < dist)
      {
         farthestDistance = dist;
      }
   }


   double max_drive_speed = 0;

   std::vector<std::vector<double>> temp_results;

   for (auto module : m_modules)
   {

      Vector2 turn_v = Vector2();
      Vector2 moduleLoc = module->getLocation(m_rotate_origin);
      

      // if all our modules are located at the rotate origin then there is no rotating we can do, if that isn't the case then calc the rotate origin
      if (!(moduleLoc[0] == 0 && moduleLoc[1] == 0))
      {
         // get optimum turning velocity for the module. Then scale it by RCW and the modules relative distance to the rot origin and the farthest module.
         // @TODO it's a bit redundant to recalc the module distances again. Would it be worthwhile to save the previous results and use them again here?
         turn_v = module->calculateOptimumTurnVelocity(m_rotate_origin) * RCW * (moduleLoc.magnitude() / farthestDistance);

      }
      
      

      Vector2 complete_vector = turn_v + trans_v;
      

      

     
      double new_speed = complete_vector.magnitude();

      // check max_drive_speed with new speed because we will need the max for later
      if (new_speed > max_drive_speed)
      {
         max_drive_speed = new_speed;
      }

      //convert to angle where 0.0 == +y axis and clockwise is positive. also add 180 so its in the range [0, 360] not its usual [-180, 180] range
      double new_heading = atan2(complete_vector[0]*-1, complete_vector[1]*-1) * (180 / M_PI) + 180;
     
   
      temp_results.push_back({new_speed, new_heading});



      
      

   }

   // normalize values to the largest one so that we aren't powering more than 100% of a motors output
   double divider = max_drive_speed <= 1 ? 1 : max_drive_speed;


   for (int i = 0; i < numModules; i++)
   {
      
      m_modules[i]->setVelocity(temp_results[i][0]/divider, temp_results[i][1]);

   
     // std::cout<< "  " + std::to_string(i) + ":" + std::to_string(m_modules[i]->getCurrentAngle(false)) + ">" + std::to_string(temp_results[i][1]);


   }

 //  std::cout << std::endl;

   


   

}


//////////////////////////////////////////////////////////////////////////
Vector2
SwerveDrive::getDisplacement(double gyro_angle)
{
   double dir_rad = -1 * (M_PI / 180) * DegreeCalc::normalizeDegree(gyro_angle);// cpp takes radians in cos, sin, and etc. So convert it to radians

   
   // return a rotated displacement rotating back to field centric (hense the * -1 above)
   return getDisplacement().getRotated(dir_rad);

}

//////////////////////////////////////////////////////////////////////////
void
SwerveDrive::updateState(void)
{

   for (int i = 0; i < m_modules.size(); i++)
   {
      m_modules[i]->updateState();

   }


}

//////////////////////////////////////////////////////////////////////////
Vector2
SwerveDrive::getDisplacement(void)
{

   Vector2 combined_displacement;


   for (int i = 0; i < m_modules.size(); i++)
   {
      // get module current displacement vector 
      combined_displacement += m_modules[i]->getDisplacement();

   }


   return combined_displacement;
   
}



//////////////////////////////////////////////////////////////////////////
Vector2
SwerveDrive::getDesiredTranslateOutput(void)
{
  

   return {0, 0};

}

//////////////////////////////////////////////////////////////////////////
double
SwerveDrive::getDesiredRotateOutput(void)
{

  /* double sum = 0;
   for (auto module : m_modules)
   {
     sum += module->calculateOptimumTurnVelocity(m_rotate_origin).getPolar()[0];

   }
*/
   return 0;

}

//////////////////////////////////////////////////////////////////////////
void
SwerveDrive::setDriftOffset(double value)
{
   m_driftOffset = value;

}




} // END namespace Drivebase
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////