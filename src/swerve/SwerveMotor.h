
// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2020 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

#ifndef __SwerveMotor_h__
#define __SwerveMotor_h__


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// class to represent a motor in a swerve module. Must have a encoder (getTicks())
/// and have a setable speed (set())
////////////////////////////////////////////////////////////////////////////////
class SwerveMotor
{

    public:

        ////////////////////////////////////////////////////////////////////////////////
        /// @brief
        /// sets the motor speed
        ////////////////////////////////////////////////////////////////////////////////
        virtual void set(double speed);

        ////////////////////////////////////////////////////////////////////////////////
        /// @brief
        /// gets the ticks/rotations of the motor
        ////////////////////////////////////////////////////////////////////////////////
        virtual double getTicks();
        
        ////////////////////////////////////////////////////////////////////////////////
        /// @brief
        /// returns the speed of the motor
        ////////////////////////////////////////////////////////////////////////////////
        virtual double getSpeed();




};



}

#endif // __SwerveMotor_h__