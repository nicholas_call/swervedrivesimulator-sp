
// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////

#include <cstdio>
#include <math.h>  



// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// class with static functions to calculate degree things
////////////////////////////////////////////////////////////////////////////////
class DegreeCalc
{

    public:

    ////////////////////////////////////////////////////////////////////////////////
    /// @brief
    /// normalizes any double into 0 - 360
    ////////////////////////////////////////////////////////////////////////////////
    static double normalizeDegree(double degree);


    ////////////////////////////////////////////////////////////////////////////////
    /// @brief
    /// generic normalize function, note should probably be in a less specfic
    /// location than DegreeCalc
    ///
    /// Normalizes any number to an arbitrary range 
    /// by assuming the range wraps around when going below min or above max 
    /// src from:https://stackoverflow.com/questions/1628386/normalise-orientation-between-0-and-360
    /// output is within [start, end)
    ////////////////////////////////////////////////////////////////////////////////
    static double normalize(const double value, const double start, const double end);
   


    ////////////////////////////////////////////////////////////////////////////////
    /// @brief
    /// gets smallest distance between two degrees 
    ////////////////////////////////////////////////////////////////////////////////
    static double getAngleDistance(double a, double b);


};




}