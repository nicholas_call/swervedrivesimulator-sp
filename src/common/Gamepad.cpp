// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////

#include <cstdio>
#include <math.h>  
#include "Gamepad.h"



/////////////////////////////////////////////////////////////////////////
Gamepad::Gamepad(void)
{
  
}


/////////////////////////////////////////////////////////////////////////
bool
Gamepad::connect(int port)
{       
    m_connectedId = -1;
   
 
	for (DWORD i = 0; i < XUSER_MAX_COUNT && m_connectedId == -1; i++)
	{
		XINPUT_STATE state;
		ZeroMemory(&state, sizeof(XINPUT_STATE));
 

        bool already_occupied = false;//(std::find(static_connected_ports.begin(), static_connected_ports.end(), i) != static_connected_ports.end());


		if (XInputGetState(i, &state) == ERROR_SUCCESS && !already_occupied)
		{
			m_connectedId = i;

            if (!m_incognito)
            {
              //  Gamepad::static_connected_ports.push_back(m_connectedId);
            }


            return true;
		}

		
	}

    return false;
}


/////////////////////////////////////////////////////////////////////////
int
Gamepad::getConnectedId(void)
{  
    return m_connectedId;
}





/////////////////////////////////////////////////////////////////////////
XINPUT_STATE
Gamepad::getState(void)
{  

	// get controller inputs
	XINPUT_STATE state;
	ZeroMemory(&state, sizeof(XINPUT_STATE));
 
	
    return state;
    
  
    
}


////////////////////////////////////////////////////////////////////////////////
float
Gamepad::applyDeadZone(float value, float deadzone)
{
	float temp = (abs(value) < deadzone ? 0 : (abs(value) - deadzone) * (value / abs(value)));

	if (deadzone > 0) temp /= 1 - deadzone;

	return temp;
}
