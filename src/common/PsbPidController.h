// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2013-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#pragma once

class PsbPidController
{
   public:
      PsbPidController(float kp,
                       float ki,
                       float kd,
                       float setPoint,
                       float minInput,       float maxInput,
                       float minOutput,      float maxOutput,
                       float minIntegralSum, float maxIntegralSum,
                       float innerEpsilon,   float outerEpsilon, bool continuous);
      virtual ~PsbPidController(void);
      
      
   public:
      void enable(void);
      void disable(void);
      void setSetPoint(float setPoint);
      void setConfiguration(float kp,
                            float ki,
                            float kd,
                            float setPoint,
                            float minInput,       float maxInput,
                            float minOutput,      float maxOutput,
                            float minIntegralSum, float maxIntegralSum,
                            float innerEpsilon,   float outerEpsilon, bool continuous);
      float calcPid(float processVariable);
      bool isOnTarget(void);
      
      
   private:
      float a_kp;                     // Proportional Gain Constant
      float a_ki;                     // Integral Gain Constant
      float a_kd;                     // Differential Gain Constant
      float a_setPoint;               // Process Set Point
      float a_minInput;               // Minimum Input Value
      float a_maxInput;               // Maximum Input Value
      float a_minOutput;              // Minimum Output Value
      float a_maxOutput;              // Maximum Output Value
      float a_minIntegralSum;         // Minimum value of accumulated integral
      float a_maxIntegralSum;         // Maximum value of accumulated integral
      float a_innerEpsilon;           // If |e(t)| < a_innerEpsilon, then onTarget
      float a_outerEpsilon;           // If |e(t)| < a_outerEpsilon, then enable integral term
      
      bool  a_isEnabled;              // Whether or not the PID controller is enabled
      bool  a_isOnTarget;             // True when the error term is within the inner epsilon region
      bool  a_continuous;             // Whether or not the PID controller will treat the range like a circle when turning the fastest direction
      float a_inputRange;             // the range in which inputs exist, needed to calc continuous 
      float a_currentProcessValue;    // pv(t) this loop, taken from calcPid() input
      float a_previousProcessValue;   // pv(t) last loop, taken from calcPid() input last time
      float a_integralSum;            // Current value of the integration of e(t)
      
      
   private:
      PsbPidController(void);
      PsbPidController(const PsbPidController& rc_rhs);
      const PsbPidController& operator=(const PsbPidController& rc_rhs);
};

