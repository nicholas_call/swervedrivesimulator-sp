#include "BSwerveDrive.h"
#include "../../SharedMemory/b3RobotSimulatorClientAPI_NoGUI.h"
#include <vector>
#include "swerve/SwerveModule.h"
#include "BSwerveMotor.h"
#include "common_sim/RobotSimulator.h"
#include "swerve/SwerveMotor.h"
#include "../../../src/Bullet3Common/b3HashMap.h"



////////////////////////////////////////////////////////////////////////////////
BSwerveDrive::BSwerveDrive()
{

}


////////////////////////////////////////////////////////////////////////////////
BSwerveDrive::BSwerveDrive(class b3RobotSimulatorClientAPI_NoGUI* sim, int uniqueId, std::vector<BSwerveModuleArgs> modules) 
	:
	m_uniqueId(uniqueId),
	m_sd_controller(new Psb::Drivebase::SwerveDrive())
{
	 


	std::map<std::string, int> index_Map = RobotSimulator::mapNameToId(sim, m_uniqueId);


	// figure out ids to each swerve module
	for (int i = 0; i < modules.size(); i++)
	{
		
		
		int drive_index = index_Map[modules[i].m_drive_joint_name];
		int rotate_index = index_Map[modules[i].m_rotate_joint_name];

		BSwerveMotor *rot_motor = new BSwerveMotor(rotate_index, modules[i].m_rotate_motor_max_velocity, modules[i].m_rotate_motor_max_torque);
		BSwerveMotor *drive_motor = new BSwerveMotor(drive_index, modules[i].m_drive_motor_max_velocity, modules[i].m_drive_motor_max_torque);
		
		rot_motor->setReversedState(true);//by default positive is counterclock wise in the bulletphys test but the swerve code assumes it's clockwise
		

		// figure out this modules location from the rotate joint
		b3JointInfo* info = new b3JointInfo();
		sim->getJointInfo(m_uniqueId, rotate_index, info);


		double x = -1 * info->m_parentFrame[0];// flip the original x so right is positive, (this value is correct in the urdf file but not when we look it up here for some reason. this could mean the urdf may be upside-down?)
		double y = info->m_parentFrame[1];		
		double z = info->m_parentFrame[2];

		
		printf("mapping modules - pos(%.3f, %.3f, %.3f) index(rotate(%.d), drive(%.d))\n", x, y, z, rotate_index, drive_index);
		

		
		Psb::Drivebase::SwerveModule *sm = new Psb::Drivebase::SwerveModule(Psb::Vector2(x, z), modules[i].m_rotate_pid, (Psb::SwerveMotor*)drive_motor, (Psb::SwerveMotor*)rot_motor);
		

		sm->setTicksPerRotation(3.1459*2);// the "tick" value in this simulator is a radian degree so set it to 2 * pi
		
		sm->setTickValueForForward(4.71239);// the default position of modules isn't strait forward so we need to set the offset
		m_sd_controller->addModule(sm);
	

		// create struct to keep track of the controller and its motors
		BSwerveModule sdm = BSwerveModule(
			sm
		, rot_motor
		, drive_motor
		);
		

		m_modules.push_back(sdm);
	}
}


////////////////////////////////////////////////////////////////////////////////
BSwerveDrive::~BSwerveDrive()
{
	//@TODO delete everything!
}



////////////////////////////////////////////////////////////////////////////////
void
BSwerveDrive::applySwerveControllerOuputs(b3RobotSimulatorClientAPI_NoGUI* sim)
{
	for (int i = 0; i < m_modules.size(); i++)
	{
		m_modules[i].applySwerveControllerOuputs(sim, m_uniqueId);
	}
}

////////////////////////////////////////////////////////////////////////////////
void
BSwerveDrive::getSwerveControllerInputs(b3RobotSimulatorClientAPI_NoGUI* sim)
{
	for (int i = 0; i < m_modules.size(); i++)
	{
		m_modules[i].getSwerveControllerInputs(sim, m_uniqueId);
	}
}



////////////////////////////////////////////////////////////////////////////////
Psb::Drivebase::SwerveDrive* 
BSwerveDrive::getSwerveDrive(void)
{
	return m_sd_controller;

}


