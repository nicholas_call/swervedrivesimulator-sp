// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "../../RobotSimulator/b3RobotSimulatorClientAPI.h"
#include "../Utils/b3Clock.h"
#include "common/Gamepad.h"
#include "BSwerveDrive.h"
#include "common/DegreeCalc.h"
#include <string.h>
#include <iostream>

#include <assert.h>
#include <filesystem>
#include <math.h>

#include <windows.h>
#include "shlwapi.h"
#include <xinput.h>
#include "TestySim.h"
#include "SwerveRobot.h"


#include <iostream>


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////



int main(int argc, char* argv[])
{
	
	if (argc == 1)
	{
		printf("Running with default args\n");
	}
	else if (argc == 3)
	{
		printf("Running with custom args\n");
		for (int x = 0; x < argc; x++)
		{
			std::cout<< ">" << std::string(argv[x]) << std::endl;
		}
	}
	else
	{
		printf("arg count error! please use no args to get default args or 2 args one for the robot selection and one for the field selection.\nPossible robots are basic and 2018.\nPossible field selections are basic and 2018.\n");
		return 1;
	}
	



	b3RobotSimulatorClientAPI* sim = new b3RobotSimulatorClientAPI();
	bool isConnected = sim->connect(eCONNECT_GUI);

	if (!isConnected)
	{
		std::cout << ("Cannot connect to physics server, stopping...") << std::endl;
		return -1;
	}



	// hide extra gui 
	//Can also use eCONNECT_DIRECT,eCONNECT_SHARED_MEMORY,eCONNECT_UDP,eCONNECT_TCP, for example:
	//sim->connect(eCONNECT_UDP, "localhost", 1234);
	sim->configureDebugVisualizer(COV_ENABLE_GUI, 0);
	
	//	sim->configureDebugVisualizer( COV_ENABLE_SHADOWS, 0);//COV_ENABLE_WIREFRAME
	sim->setTimeOut(10);

	//syncBodies is only needed when connecting to an existing physics server that has already some bodies
	sim->syncBodies();
	btScalar fixedTimeStep = 1. / 240.;

	

	btQuaternion q = sim->getQuaternionFromEuler(btVector3(0.1, 0.2, 0.3));
	btVector3 rpy;
	rpy = sim->getEulerFromQuaternion(q);

	sim->setGravity(btVector3(0, 0, -9.8));


	//////////////////////////////////////////////////////////
	// Figure out our path so we can add it as a search path
	
	#ifndef UNICODE  
 		typedef std::string String; 
	#else
  		typedef std::wstring String; 
	#endif


	// Get the full path of current exe file.
	TCHAR FilePath[MAX_PATH] = { 0 };
	GetModuleFileName(0, FilePath, MAX_PATH);

	// Strip the exe filename from path and get folder name.
	PathRemoveFileSpec(FilePath);    

	std::string filePath = FilePath;

	std::cout << "Current Directory: " << filePath.c_str() << std::endl;


	sim->setAdditionalSearchPath(filePath);


	////////////////////////////////////////////////////
	// Do our simulation

	TestySim* ts = new TestySim(sim);


	ts->loadField(argc == 3 ? std::string(argv[2]) : "");

	for (int x = 0; x < 1; x++)
	{
		SwerveRobot* sr = new SwerveRobot();
		sr->configure(argc == 3 ? std::string(argv[1]) : "", sim, btVector3(0, 0, x + 1), btQuaternion(0, 1.5, 0));



		ts->addRobot(sr);
	}


	ts->go(fixedTimeStep);







	////////////////////////////////////////////////////
	// Delete physics client and exit

	std::cout << ("sim->disconnect") << std::endl;

	sim->disconnect();

	std::cout << ("delete sim") << std::endl;
	delete sim;

	std::cout << ("exit") << std::endl;
}






