// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////

#include "../Utils/b3Clock.h"
#include <string.h>
#include <iostream>

#include <assert.h>
#include <filesystem>
#include <math.h>

#include <windows.h>
#include "shlwapi.h"
#include <xinput.h>

#include "TestySim.h"




//////////////////////////////////////////////////////////////////////////
TestySim::TestySim(class b3RobotSimulatorClientAPI_NoGUI* sim) : RobotSimulator(sim)
{

}



//////////////////////////////////////////////////////////////////////////
void
TestySim::loadField(std::string arg)
{

	b3RobotSimulatorLoadUrdfFileArgs field_load_args;
	field_load_args.m_startOrientation = btQuaternion(0, 0, 0);
	field_load_args.m_startPosition = btVector3(0, 0, 0);

	

	if (arg == "2018")
	{

		b3RobotSimulatorLoadUrdfFileArgs cube_load_args;
		cube_load_args.m_startOrientation = btQuaternion(0, 1.57, 0);
		cube_load_args.m_startPosition = btVector3(0, -3, 0);

	
		m_simClient->loadURDF("frc2018field/frc2018field.urdf", cube_load_args);
	}
	else
	{
		// load some landscape
		b3RobotSimulatorLoadUrdfFileArgs cube_load_args;
		cube_load_args.m_startOrientation = btQuaternion(0, 1.57, 0);
		cube_load_args.m_startPosition = btVector3(0, -3, 0);

		m_simClient->loadURDF("plane.urdf", field_load_args);
		m_simClient->loadURDF("basic_field.urdf", cube_load_args);

	}
	


	
	// load some props
	for (int x = 0; x < 6; x++)
	{
		b3RobotSimulatorLoadUrdfFileArgs cube_load_args;
		cube_load_args.m_startOrientation = btQuaternion(0, 1.57, 0);
		cube_load_args.m_startPosition = btVector3(0-x, -0.5, 2);


		int power_cubeId = m_simClient->loadURDF("power_cube.urdf", cube_load_args);

	}


	

}



//////////////////////////////////////////////////////////////////////////
void 
TestySim::go(void)
{

	go(1. / 240.);
	

}


//////////////////////////////////////////////////////////////////////////
void 
TestySim::go(double fixedTimeStep)
{
	m_simClient->setRealTimeSimulation(false);
	m_simClient->setTimeStep(fixedTimeStep);
	



	while (m_simClient->canSubmitCommand())
	{

		runRobotTasks();


		m_simClient->stepSimulation();



		b3Clock::usleep(1000. * 1000. * fixedTimeStep);
	}
}



