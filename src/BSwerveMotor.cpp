// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////

#include <cstdio>
#include <math.h>  
#include "BSwerveMotor.h"
#include "../../../SharedMemory/b3RobotSimulatorClientAPI_NoGUI.h"


/////////////////////////////////////////////////////////////////////////
BSwerveMotor::BSwerveMotor(int joint_index, double max_velocity, double max_torque):
	m_joint_index(joint_index),
	m_max_torque(max_torque),
	m_max_velocity(max_velocity)
{
	// intentionaly left empty
}

/////////////////////////////////////////////////////////////////////////
void 
BSwerveMotor::set(double speed) 
{
	m_speed = speed * m_max_velocity;
}

/////////////////////////////////////////////////////////////////////////
double
BSwerveMotor::getTicks()
{
	if (getReversedState())
	{
		return m_ticks * -1;
	}
	else
	{
		return m_ticks;
	}
	
}

/////////////////////////////////////////////////////////////////////////
double
BSwerveMotor::getSpeed()
{
	if (getReversedState())
	{
		return m_speed * -1;
	}
	else
	{
		return m_speed;
	}
	
}

/////////////////////////////////////////////////////////////////////////
void 
BSwerveMotor::applyOutputs(b3RobotSimulatorClientAPI_NoGUI* sim, int body_id)
{
	//set joint speed
	b3RobotSimulatorJointMotorArgs rotate_args = b3RobotSimulatorJointMotorArgs(0);
	rotate_args.m_maxTorqueValue = m_max_torque;
 	rotate_args.m_targetVelocity = getReversedState() ? m_speed * -1 : m_speed;

	sim->setJointMotorControl(body_id, m_joint_index, rotate_args);
}

/////////////////////////////////////////////////////////////////////////
void 
BSwerveMotor::getInputs(b3RobotSimulatorClientAPI_NoGUI* sim, int body_id)
{
	// get joint info
	b3JointSensorState* rotate_info = new b3JointSensorState();
	sim->getJointState(body_id, m_joint_index, rotate_info);
	
	m_ticks = rotate_info->m_jointPosition;
}



/////////////////////////////////////////////////////////////////////////
void
BSwerveMotor::setReversedState(bool value)
{
    m_reversed = value;
}

/////////////////////////////////////////////////////////////////////////
bool
BSwerveMotor::getReversedState()
{
    return m_reversed;
}

/////////////////////////////////////////////////////////////////////////
void
BSwerveMotor::toggleReversedState()
{
    if (m_reversed)
    {
        m_reversed = false;
    }
    else
    {
        m_reversed = true;
    }
    
}

