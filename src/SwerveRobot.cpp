// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "../../../RobotSimulator/b3RobotSimulatorClientAPI.h"
#include "../Utils/b3Clock.h"
#include "common/Gamepad.h"
#include <string.h>
#include <iostream>

#include <assert.h>
#include <filesystem>
#include <math.h>

#include <windows.h>
#include "shlwapi.h"
#include <xinput.h>

#include "SwerveRobot.h"
#include "BSwerveDrive.h"
#include "common_sim/Robot.h"
#include "common_sim/RobotSimulator.h"


//////////////////////////////////////////////////////////////////////////
SwerveRobot::SwerveRobot(void)
{
	// intentionally left empty
}


//////////////////////////////////////////////////////////////////////////
void 
SwerveRobot::setArmPos(b3RobotSimulatorClientAPI_NoGUI* sim, int jointId, float pos)
{
	b3RobotSimulatorJointMotorArgs args = b3RobotSimulatorJointMotorArgs(2);
	args.m_targetPosition = pos;
	args.m_kp = 0.08;
	args.m_maxTorqueValue = 60;//@TODO resonable value?
		
	sim->setJointMotorControl(m_id, jointId, args);
}

//////////////////////////////////////////////////////////////////////////
void 
SwerveRobot::setRampSqueeze(b3RobotSimulatorClientAPI_NoGUI* sim, int jointId, float pos)//this works but really is redundant /
{
	b3RobotSimulatorJointMotorArgs args = b3RobotSimulatorJointMotorArgs(2);
	args.m_targetPosition = pos;
	args.m_kp = 0.08;
	args.m_maxTorqueValue = 100;//@TODO resonable value?
		
	sim->setJointMotorControl(m_id, jointId, args);
}

//////////////////////////////////////////////////////////////////////////
void
SwerveRobot::setRollerSpeed(b3RobotSimulatorClientAPI_NoGUI* sim, std::vector<int> jointIds, float speed)
{
	b3RobotSimulatorJointMotorArgs rotate_args = b3RobotSimulatorJointMotorArgs(0);
	rotate_args.m_maxTorqueValue = 30;//@todo resonable value?
 	rotate_args.m_targetVelocity = speed;

	for (int i : jointIds) 
	{
		sim->setJointMotorControl(m_id, i, rotate_args);
	}
	
}

//////////////////////////////////////////////////////////////////////////
void 
SwerveRobot::runTasks(b3RobotSimulatorClientAPI_NoGUI* sim)
{


	// get virtual bullet physics inputs for the sd
	m_bsdController->getSwerveControllerInputs(sim);






	



	//////////////////////////////////////////////////////////
	// get gyro angle
	btVector3 robot_position;
	btQuaternion robot_orientation;
	sim->getBasePositionAndOrientation(m_id, robot_position, robot_orientation);
	//std::cout <<"Real: X:" + std::to_string(robot_position[0]) + ",\t Z:" +  std::to_string(robot_position[1]) + "\t estimated:";

	// get euler of orientation
	btScalar robot_yaw;
	btScalar robot_pitch;
	btScalar robot_roll;
	robot_orientation.getEulerZYX(robot_yaw, robot_pitch, robot_roll);

	double gyro_angle = 360 - (robot_yaw * 57.296 + 180) + m_field_centric_offset;// convert to degrees, stop it from going -180 to 180, reverse it so its clockwise
	//printf("gryo:%.3f\n", gyro_angle);



	// @DEBUG displacement calc:
	//m_encoder_sum += m_sd_controller->getDisplacement(0);
	//std::cout << "displacement in forward" << std::to_string(m_encoder_sum) << std::endl;

	m_displacement_sum += m_sd_controller->getDisplacement(gyro_angle);


	// print estimated location
	//std::cout << " x:" << std::to_string((double)((int)(m_displacement_sum[0] * 1000.0 )) / 1000.0) + ",\t z:" + std::to_string((double)((int)(m_displacement_sum[1] * 1000.0 )) / 1000.0) << std::endl;




	// default inputs
	float leftStickX = 0;
	float leftStickY = 0;
	float rightStickX = 0;

	
	float rightArmPos = 0;
	float leftArmPos = 0;
	


	///////////////////////////////////////////////////////////
	// Get oi Inputs for our simulation
	XINPUT_STATE state = m_gamepad->getState();
 
	if (XInputGetState(m_gamepad->getConnectedId(), &state) == ERROR_SUCCESS)// if still connected
	{
		// Extract raw datad
		float normLX = fmaxf(-1, (float) state.Gamepad.sThumbLX / 32767);
		float normLY = fmaxf(-1, (float) state.Gamepad.sThumbLY / 32767);

		float normRX = fmaxf(-1, (float) state.Gamepad.sThumbRX / 32767);
		//float normRY = fmaxf(-1, (float) state.Gamepad.sThumbRY / 32767);
		float normRightArmPos = fmaxf(0.0, (float) state.Gamepad.bRightTrigger / 255);
		float normLeftArmPos = fmaxf(0.0, (float) state.Gamepad.bLeftTrigger / 255);


		// apply dead zones
		leftStickX = Gamepad::applyDeadZone(normLX, m_analog_stick_deadzone);
		leftStickY = Gamepad::applyDeadZone(normLY, m_analog_stick_deadzone);
		
		rightStickX = Gamepad::applyDeadZone(normRX, m_analog_stick_deadzone);


		rightArmPos = Gamepad::applyDeadZone(normRightArmPos, m_analog_stick_deadzone) * 3.145 - 3.145/2;
		leftArmPos = Gamepad::applyDeadZone(normLeftArmPos, m_analog_stick_deadzone) * -3.145 + 3.145/2;

		// do arm positions
		setArmPos(sim, m_right_arm_rotate_motor_id, rightArmPos);
		setArmPos(sim, m_left_arm_rotate_motor_id, leftArmPos);

		


		//@TODO add proper camera controls  and translate controls because it is a thing
		
		
		
//(((state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) == 512))
//(((state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) == 256))
		
		// toggle camera follow @TODO rising edge isn't working??
		if ((state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) == 512)
		{

			if (!m_prev_rightShoulder)
			{
				if (m_camera_follow)
				{
					m_camera_follow = false;
				}
				else
				{
					m_camera_follow = true;
				}
			}
			

			m_prev_rightShoulder = true;
		}
		else 
		{
			m_prev_rightShoulder = false;
		}
		
		// toggle field centric driving @TODO rising edge isn't working??
		if ((state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) == 256)
		{
			
			if (!m_prev_leftShoulder)
			{
				if (m_field_centric_driving)
				{

					std::cout << "Switching to field centric driving" << std::endl;
					m_field_centric_driving = false;
				}
				else
				{

					std::cout << "Switching to robot centric driving" << std::endl;
					m_field_centric_driving = true;
				}
			}
			

			m_prev_leftShoulder = true;


		}
		else
		{
			m_prev_leftShoulder = false;
		}


		// handel using the dpad to change the field oriented direction 
		if (((state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) > 0))
		{
			m_field_centric_offset = 90;
		}
		else if (((state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) > 0))
		{
			m_field_centric_offset = 180;
		}
		else if (((state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) > 0))
		{
			m_field_centric_offset = 270;
		}
		else if (((state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) > 0))
		{
			m_field_centric_offset = 0;
		}


		if (m_is2018Bot)
		{

		
			// run the arm rollers
			if (((state.Gamepad.wButtons & XINPUT_GAMEPAD_X) > 0))
			{	
				setRollerSpeed(sim, m_right_arm_roller_ids, -23);
				setRollerSpeed(sim, m_left_arm_roller_ids, 23);

				setRampSqueeze(sim, m_left_ramp_squeeze_id, .4);
				setRampSqueeze(sim, m_right_ramp_squeeze_id, -.4);
			}
			else
			{

				setRampSqueeze(sim, m_left_ramp_squeeze_id, -.4);
				setRampSqueeze(sim, m_right_ramp_squeeze_id, .4);

				setRollerSpeed(sim, m_right_arm_roller_ids, 0);
				setRollerSpeed(sim, m_left_arm_roller_ids, 0);
			}




			
			// run the shoot rollers
			if (((state.Gamepad.wButtons & XINPUT_GAMEPAD_A) > 0))
			{	
				setRollerSpeed(sim, m_right_ramp_roller_ids, -60);
				setRollerSpeed(sim, m_left_ramp_roller_ids, 60);

			}
			else if (((state.Gamepad.wButtons & XINPUT_GAMEPAD_B) > 0))
			{	
				setRollerSpeed(sim, m_right_ramp_roller_ids, -30);
				setRollerSpeed(sim, m_left_ramp_roller_ids, 30);

			}
			else if (((state.Gamepad.wButtons & XINPUT_GAMEPAD_Y) > 0))
			{	
				setRollerSpeed(sim, m_right_ramp_roller_ids, 30);
				setRollerSpeed(sim, m_left_ramp_roller_ids, -30);

			}
			else
			{
				setRollerSpeed(sim, m_right_ramp_roller_ids, 0);
				setRollerSpeed(sim, m_left_ramp_roller_ids, 0);
			}
	
		}

	}
	else// take keyboard input
	{
	


		b3KeyboardEventsData keyEvents;
		sim->getKeyboardEvents(&keyEvents);
		if (keyEvents.m_numKeyboardEvents)
		{
			//printf("num key events = %d]\n", keyEvents.m_numKeyboardEvents);
			
			//m_keyState is a flag combination of eButtonIsDown,eButtonTriggered, eButtonReleased
			for (int i = 0; i < keyEvents.m_numKeyboardEvents; i++)
			{
				b3KeyboardEvent& e = keyEvents.m_keyboardEvents[i];
				//std::cout << (char) e.m_keyCode << std::endl;
				

				// handle camera follow
				if (e.m_keyCode == 'c')
				{
					if (!m_prev_rightShoulder)
					{
						if (m_camera_follow)
						{
							m_camera_follow = false;
						}
						else
						{
							m_camera_follow = true;
						}
					}
					
					m_prev_rightShoulder = true;
				}
				else
				{
					m_prev_rightShoulder = false;
				}

				// handle centric driving switching
				if (e.m_keyCode == 't')
				{
					if (!m_prev_leftShoulder)
					{
						if (m_field_centric_driving)
						{
							m_field_centric_driving = false;
						}
						else
						{
							m_field_centric_driving = true;
						}
					}
					
					
					m_prev_leftShoulder = true;
					
				}
				else
				{
					m_prev_leftShoulder = false;
				}
			


				// handle fwd
				if (e.m_keyCode == 'u')
				{
					
					leftStickY = 1;
					
				}
				else if (e.m_keyCode == 'j')
				{
					
					leftStickY = -1;
					
				}


				// handle str
				if (e.m_keyCode == 'h')
				{
					
					leftStickX = -1;
					
				}
				else if (e.m_keyCode == 'k')
				{
					
					leftStickX = 1;
					
				}

				// handle rcw
				if (e.m_keyCode == 'z')
				{
					
					rightStickX = -1;
					
				}
				else if (e.m_keyCode == 'x')
				{
					
					rightStickX = 1;
					
				}


				//normalize translate vector 
				if (leftStickX != 0 && leftStickY != 0)
				{
					double r = sqrt(pow(leftStickX, 2) + pow(leftStickY, 2));
					leftStickX /= r;
					leftStickY /= r;
					
				}
			

			}

		}



		m_gamepad->connect();// try connecting a controller
	}



	if (m_camera_follow)
	{

		b3OpenGLVisualizerCameraInfo* camer_info = new b3OpenGLVisualizerCameraInfo();
		sim->getDebugVisualizerCamera(camer_info);
		sim->resetDebugVisualizerCamera(camer_info->m_dist, camer_info->m_pitch, camer_info->m_yaw, robot_position);

	}



	
	// apply controller output
	m_sd_controller->set(leftStickX, leftStickY, rightStickX, (m_field_centric_driving) ? gyro_angle : NULL);



	
		
	m_bsdController->applySwerveControllerOuputs(sim);


	// figure out current orientations of modules and their encoder counts for prev values
	m_sd_controller->updateState();

	
}

//////////////////////////////////////////////////////////////////////////
void SwerveRobot::configure(std::string arg, b3RobotSimulatorClientAPI_NoGUI* sim, btVector3 starting_location, btQuaternion starting_orientation)
{


	/////////////////////////////////////////////////////////
	// Create the Swerve drive
	const int num_modules = 4;
	
	// define the swerve drive spawn
	b3RobotSimulatorLoadUrdfFileArgs load_args;
	load_args.m_startOrientation = starting_orientation;
	load_args.m_startPosition = starting_location;



	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// figure out which model to load. This class is configured to work for either a basic just swerve robot or a approximation of
	// 4061's 2018 robot but with a swerve drive

	if (arg == "2018")
	{
		m_id = sim->loadURDF("4061_2018_swerve_bot.urdf", load_args);
		m_is2018Bot = true;
	}
	else
	{
		m_id = sim->loadURDF("swerve_robot_2.urdf", load_args);
	}
	





	// define the names of the module motors in our urdf and their associated pid controller
	std::vector<BSwerveModuleArgs> modules;
	for (int i = 0; i < num_modules; i++)
	{
		PsbPidController* rotate_pid = Psb::Drivebase::SwerveModule::createPID(/*0.01*/ 0.01, 0, 0, -1.0, 1.0, 1, 10, 0.1, 6);


		modules.push_back(BSwerveModuleArgs(std::to_string(i) + "_SM_motor_rotate", std::to_string(i) + "_SM_motor_drive", 60, 60, 60, 60, rotate_pid));
	}
	


	// create bullet physics swerve drive controller
	m_bsdController = new BSwerveDrive(sim, m_id, modules);
	m_sd_controller = m_bsdController->getSwerveDrive();
	m_sd_controller->setEnabled(true);
	//m_sd_controller->setRotateOrigin(Psb::Vector2(0, 0.3));
	m_sd_controller->setDriftOffset(0.085);
	


	std::map<std::string, int> robot_map = RobotSimulator::mapNameToId(sim, m_id);

	// configure 2018 bot motors
	if (m_is2018Bot)
	{

		// arm rollers and rotate motors
		m_left_arm_rotate_motor_id = robot_map["arm_left_holder"];
		m_right_arm_rotate_motor_id = robot_map["arm_right_holder"];

		m_left_arm_roller_ids.push_back(robot_map["arm_left_wheel_0_holder"]);
		m_left_arm_roller_ids.push_back(robot_map["arm_left_wheel_1_holder"]);
		m_left_arm_roller_ids.push_back(robot_map["arm_left_wheel_2_holder"]);

		m_right_arm_roller_ids.push_back(robot_map["arm_right_wheel_0_holder"]);
		m_right_arm_roller_ids.push_back(robot_map["arm_right_wheel_1_holder"]);
		m_right_arm_roller_ids.push_back(robot_map["arm_right_wheel_2_holder"]);
		m_right_arm_roller_ids.push_back(robot_map["ramp_roller_holder"]);//cheatly add the extra roller on the ramp 

		//ramp rollers
		m_left_ramp_roller_ids.push_back(robot_map["ramp_left_wheel_0_holder"]);
		m_left_ramp_roller_ids.push_back(robot_map["ramp_left_wheel_1_holder"]);
		m_left_ramp_roller_ids.push_back(robot_map["ramp_left_wheel_2_holder"]);
		

		m_right_ramp_roller_ids.push_back(robot_map["ramp_right_wheel_0_holder"]);
		m_right_ramp_roller_ids.push_back(robot_map["ramp_right_wheel_1_holder"]);
		m_right_ramp_roller_ids.push_back(robot_map["ramp_right_wheel_2_holder"]);
		

		// setup ramp hold cube suspension	
		/*b3RobotSimulatorJointMotorArgs args = b3RobotSimulatorJointMotorArgs(2);
		args.m_maxTorqueValue = 20;

		args.m_targetPosition = -0.04;
		sim->setJointMotorControl(m_id, robot_map["left_ramp_compress_holder"], args);

		args.m_targetPosition = 0.04;
		sim->setJointMotorControl(m_id, robot_map["right_ramp_compress_holder"], args);*/
		m_left_ramp_squeeze_id = robot_map["left_ramp_compress_holder"];
		m_right_ramp_squeeze_id = robot_map["right_ramp_compress_holder"];


	
	}
	


	// increase friction on wheels of our robot
	/*
	for (int i = 0; i < 4; i++)
	{
		int wheelId = robot_map[std::to_string(i) + "_SM_wheel"];

		// get current dynamics
		b3DynamicsInfo* get_info = new b3DynamicsInfo();
		sim->getDynamicsInfo(m_id, wheelId, get_info);
		printf("contactDamping:%.3f", get_info->m_contactDamping);
		

		b3RobotSimulatorChangeDynamicsArgs args;
     	
		args.m_contactStiffness = 1.0;
		args.m_lateralFriction = 1;
		args.m_frictionAnchor = 1000000;
		
		//args.m_contactDamping = -10000;
	   // args.m_rollingFriction = 1000;
		sim->changeDynamics(m_id, wheelId, args);
		
	}
*/

	// setup suspension	
	for (int i = 0; i < num_modules; i++)
	{
		int susId = robot_map[std::to_string(i) + "_SM_suspension"];

		b3RobotSimulatorJointMotorArgs args = b3RobotSimulatorJointMotorArgs(2);
		args.m_targetPosition = -0.04;
		
		sim->setJointMotorControl(m_id, susId, args);
		
	}



	///////////////////////////////////////////////////////
	// Configure OI

	// Find a controller with xinput
	m_gamepad = new Gamepad();
	m_gamepad->connect();

	

	
	
}

   

