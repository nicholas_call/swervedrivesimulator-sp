#ifndef b_swerve_drive_h
#define b_swerve_drive_h

#include "LinearMath/btVector3.h"
#include "../../src/LinearMath/btQuaternion.h"
#include "../../../src/Bullet3Common/b3HashMap.h"
#include "../../SharedMemory/b3RobotSimulatorClientAPI_NoGUI.h"
#include <vector>
#include <map>
#include "swerve\SwerveModule.h"
#include "swerve\SwerveDrive.h"
#include "BSwerveMotor.h"
#include <iostream>



struct BSwerveModuleArgs
{
	public:


		// urdf name for joints that will act as our motors
		std::string m_rotate_joint_name;
		std::string m_drive_joint_name;


		// motor data
		double m_drive_motor_max_velocity;
		double m_drive_motor_max_torque;
		double m_rotate_motor_max_velocity;
		double m_rotate_motor_max_torque;


		PsbPidController* m_rotate_pid;


		BSwerveModuleArgs(
			std::string rotate_joint_name,
			std::string drive_joint_name,
			double drive_motor_max_velocity,
			double drive_motor_max_torque,
			double rotate_motor_max_velocity,
			double rotate_motor_max_torque,
			PsbPidController* rotate_pid)
			:
			m_rotate_joint_name(rotate_joint_name),
			m_drive_joint_name(drive_joint_name),
			m_drive_motor_max_velocity(drive_motor_max_velocity),
			m_drive_motor_max_torque(drive_motor_max_torque),
			m_rotate_motor_max_velocity(rotate_motor_max_velocity),
			m_rotate_motor_max_torque(rotate_motor_max_torque),
			m_rotate_pid(rotate_pid)
		{};

};



struct BSwerveModule
{
	public:

		// motors for this module
		BSwerveMotor* m_rotate_motor;
		BSwerveMotor* m_drive_motor;
		


		// our pointer to the controller of this module
		Psb::Drivebase::SwerveModule* m_module;

		

		// gets angles from bullet physics for our motors and use those as encoders
		void getSwerveControllerInputs(class b3RobotSimulatorClientAPI_NoGUI* sim, int bodyId)
		{	

			m_rotate_motor->getInputs(sim, bodyId);
			m_drive_motor->getInputs(sim, bodyId);

			// get drive info			
		/*	b3JointSensorState * drive_info = new b3JointSensorState();
			sim->getJointState(bodyId, m_drive_joint_index, drive_info);
	

			m_module->setDriveEncoderCounts(drive_info->m_jointPosition);




			// get rotate info
			b3JointSensorState * rotate_info = new b3JointSensorState();
			sim->getJointState(bodyId, m_rotate_joint_index, rotate_info);
	

			m_module->setRotateEncoderCounts(360 - (rotate_info->m_jointPosition * 57.2957795131)); // convert radien to 360 degrees and swap it to be clockwise
		*/

			//@DEBUG stuff
		/*	if (m_rotate_joint_index == 7)
			{
				//std::cout<< "drivePosition:" + std::to_string(drive_info->m_jointPosition) << std::endl;
				//printf("module%d%d angle %.3f -> %.3f rotate:%.3f drive:%.3f\n", m_rotate_joint_index, m_drive_joint_index, m_module->getCurrentAngle(), m_module->getDesiredAngle(), m_module->getRotateMotor(), m_module->getDriveMotor());
			}*/
		
		};


		// applies m_modules result to the corresponding bullet physics joints
		void applySwerveControllerOuputs(class b3RobotSimulatorClientAPI_NoGUI* sim, int bodyId)
		{
			
			m_rotate_motor->applyOutputs(sim, bodyId);
			m_drive_motor->applyOutputs(sim, bodyId);
			// set drive motor
		/*	b3RobotSimulatorJointMotorArgs drive_args = b3RobotSimulatorJointMotorArgs(0);
			drive_args.m_targetVelocity = m_module->getDriveMotor()*m_motor_max_velocity * -1;// reverse it because the bullet physics setup by default is counterclockwise positive
			drive_args.m_maxTorqueValue = m_motor_max_torque;

			sim->setJointMotorControl(bodyId, m_drive_joint_index, drive_args);




			// set rotate motor
			b3RobotSimulatorJointMotorArgs rotate_args = b3RobotSimulatorJointMotorArgs(0);
			rotate_args.m_targetVelocity = m_module->getRotateMotor()*m_motor_max_velocity * -1;// reverse it because the bullet physics setup by default is counterclockwise positive
			rotate_args.m_maxTorqueValue = m_motor_max_torque;

			sim->setJointMotorControl(bodyId, m_rotate_joint_index, rotate_args);*/
		
		
		/*	b3RobotSimulatorJointMotorArrayArgs args = b3RobotSimulatorJointMotorArrayArgs(0, 2);
			args.m_jointIndices[0] = *new int(m_drive_joint_index);
			args.m_targetVelocities[0] = *new double(m_module->getDriveMotor()*m_motor_max_velocity);
			args.m_forces[0] = *new double(m_motor_max_torque);

			args.m_jointIndices[1] = *new int(m_rotate_joint_index);
			args.m_targetVelocities[1] = *new double(m_module->getRotateMotor()*m_motor_max_velocity);
			args.m_forces[1] = *new double(m_motor_max_torque);

			sim->setJointMotorControlArray(bodyId, args);*/


		//	if (m_rotate_joint_index == 0 && m_drive_joint_index == 2)
		//	printf("module%d%d - drive:%.3f rotate:%.3f\n", m_rotate_joint_index, m_drive_joint_index, m_module->getDriveMotor(), m_module->getRotateMotor());
		};




		BSwerveModule(Psb::Drivebase::SwerveModule* module, BSwerveMotor* rotate_motor, BSwerveMotor* drive_motor) : 
			m_rotate_motor(rotate_motor),
			m_drive_motor(drive_motor),
			m_module(module)
		{};



		
};


class BSwerveDrive
{
	
	
private:
	std::vector<BSwerveModule> m_modules;

	int m_uniqueId = -1;

	
	Psb::Drivebase::SwerveDrive* m_sd_controller;


public:



	BSwerveDrive();
	BSwerveDrive(class b3RobotSimulatorClientAPI_NoGUI* sim, int uniqueId, std::vector<BSwerveModuleArgs> modules);


	virtual ~BSwerveDrive();

	void applySwerveControllerOuputs(class b3RobotSimulatorClientAPI_NoGUI* sim);

	void getSwerveControllerInputs(class b3RobotSimulatorClientAPI_NoGUI* sim);
	

	Psb::Drivebase::SwerveDrive* getSwerveDrive(void);






};


#endif  //b_swerve_drive_h